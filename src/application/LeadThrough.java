package application;

import java.util.concurrent.TimeUnit;

import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.*;
import com.kuka.roboticsAPI.motionModel.controlModeModel.JointImpedanceControlMode;


public class LeadThrough {
    private LBR robot;
    private JointImpedanceControlMode leadThrought;
    private double DAMPING   = 100;
    private double STIFFNESS = 0;

    public LeadThrough(LBR robot) {
        this.robot = robot;

        // Inizializza i parametri del control mode
        this.leadThrought = new JointImpedanceControlMode();
        this.leadThrought.setStiffnessForAllJoints(STIFFNESS);
        this.leadThrought.setDampingForAllJoints(DAMPING);
    }

    public LeadThrough(LBR robot, double stiffness) throws IllegalArgumentException {
        this.robot = robot;

        // Inizializza i parametri del control mode
        this.leadThrought = new JointImpedanceControlMode();
        this.leadThrought.setDampingForAllJoints(DAMPING);

        try {
            this.leadThrought.setStiffnessForAllJoints(stiffness);
        } catch (IllegalArgumentException e) {
            throw e;
        }
    }

    public LeadThrough(LBR robot, double stiffness, double damping) throws IllegalArgumentException {
        this.robot = robot;

        // Inizializza i parametri del control mode
        this.leadThrought = new JointImpedanceControlMode();
        this.leadThrought.setDampingForAllJoints(DAMPING);

        try {
            this.leadThrought.setStiffnessForAllJoints(stiffness);
        } catch (IllegalArgumentException e) {
            throw e;
        }
    }

    public void setDamping(double damping) throws IllegalArgumentException {
        try {
            this.leadThrought.setDampingForAllJoints(damping);
        } catch (IllegalArgumentException e) {
            throw e;
        }
    }

    public void setStiffness(double stiffness) throws IllegalArgumentException {
        try {
            this.leadThrought.setStiffnessForAllJoints(stiffness);
        } catch (IllegalArgumentException e) {
            throw e;
        }
    }

    public void activate() {
        robot.move(positionHold(leadThrought, -1, TimeUnit.SECONDS));
    }
    
    public JointPosition getJointPosition() {
    	return robot.getCurrentJointPosition();
    }

}
