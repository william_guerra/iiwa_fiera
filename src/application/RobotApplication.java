package application;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.*;

import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.CartDOF;
import com.kuka.roboticsAPI.geometricModel.ObjectFrame;
import com.kuka.roboticsAPI.geometricModel.Tool;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianImpedanceControlMode;
import com.kuka.roboticsAPI.motionModel.controlModeModel.JointImpedanceControlMode;

/**
 * Implementation of a robot application.
 * <p>
 * The application provides a {@link RoboticsAPITask#initialize()} and a 
 * {@link RoboticsAPITask#run()} method, which will be called successively in 
 * the application lifecycle. The application will terminate automatically after 
 * the {@link RoboticsAPITask#run()} method has finished or after stopping the 
 * task. The {@link RoboticsAPITask#dispose()} method will be called, even if an 
 * exception is thrown during initialization or run. 
 * <p>
 * <b>It is imperative to call <code>super.dispose()</code> when overriding the 
 * {@link RoboticsAPITask#dispose()} method.</b> 
 * 
 * @see UseRoboticsAPIContext
 * @see #initialize()
 * @see #run()
 * @see #dispose()
 */
public class RobotApplication extends RoboticsAPIApplication {
	@Inject
	private LBR iiwa;
	
	@Inject @Named("Gripper")
	private Tool punta;
	
	private ObjectFrame tcp;
	
	private CartesianImpedanceControlMode avvitatura;
	private CartesianImpedanceControlMode muovi;
	private JointImpedanceControlMode muovimi;
	
	@Override
	public void initialize() {
		punta.attachTo(iiwa.getFlange());
		
		tcp = punta.getFrame("/punta");
		
		//tcp = gripper.getFrame("punta");
		
		muovi = new CartesianImpedanceControlMode();
		muovi.setDampingToDefaultValue();
		muovi.parametrize(CartDOF.TRANSL).setStiffness(100);
		
		muovimi = new JointImpedanceControlMode(7);
		muovimi.setDampingForAllJoints(0.2);
		muovimi.setDamping(0.2, 0.2, 0.2, 0.2, 0.2, 0.1, 0.1);
		muovimi.setStiffnessForAllJoints(50);
		
		avvitatura = new CartesianImpedanceControlMode();
		avvitatura.parametrize(CartDOF.TRANSL).setStiffness(1000);
	}

	@Override
	public void run() {
		// your application execution starts here
		// Ritorno in home
		//iiwa.setHomePosition(getApplicationData().getFrame("/P1"));
		
		//iiwa.move(ptp(getApplicationData().getFrame("/P1")));
		
		//iiwa.move(lin(getApplicationData().getFrame("/P3")).setCartVelocity(100).setCartAcceleration(0.1).);
		//iiwa.move(lin)
		iiwa.move(positionHold(muovimi, -1, TimeUnit.SECONDS) );
		//iiwa.move(positionHold(avvitatura, -1, TimeUnit.SECONDS) );
	}
}